package com.crossover.test;

public class LinkedList {
	
	private Node head;
	private int listCount;
	
	public LinkedList() {
		head = new Node(null);
		listCount = 0;
	}
	
	public void add(Object data) {
        Node temp = new Node(data);
        Node current = head;
        
        while (current.getNext() != null) { // starting at the head node, crawl to the end of the list
            current = current.getNext();
        }

        current.setNext(temp); // the last node's "next" reference set to our new node
        listCount++;// increment the number of elements variable
    }	
	
	public void add(Object data, int index) {  // inserts the specified element at the specified position in this list
		
        Node temp = new Node(data);
        Node current = head;
        
        for (int i = 1; i < index && current.getNext() != null; i++) { // crawl to the requested index or the last element in the list, whichever comes first
            current = current.getNext();
        }
        
        temp.setNext(current.getNext()); // set the new node's next-node reference to this node's next-node reference
        
        current.setNext(temp); // now set this node's next-node reference to the new node
        
        listCount++;// increment the number of elements variable
    }
	
	
	 public Object get(int index) { // returns the element at the specified position in this list.
	        
		 if (index <= 0) { // index must be 1 or higher
			 return null;
		 }
	 
		 Node crunchifyCurrent = head.getNext();
	        
		 for (int i = 1; i < index; i++) {
	        	
			 if (crunchifyCurrent.getNext() == null) {
				 return null;
			 }
	 
			 crunchifyCurrent = crunchifyCurrent.getNext();
		 }
	   
		 return crunchifyCurrent.getData();   
	 }
	 
	 
	 public boolean remove(int index) { // removes the element at the specified position in this list.
	    
		 if (index < 1 || index > size()) {
			 return false;
		 }
	 
		 Node current = head;
	        
		 for (int i = 1; i < index; i++) {
			 
			 if (current.getNext() == null) {
				 return false;
			 }
	 
			 current = current.getNext();
		 }
		 
		 current.setNext(current.getNext().getNext());
		 listCount--; // decrement the number of elements variable
		 return true;
	 }
	 
	 
	 public int size() { // returns the number of elements in this list.
	    return listCount;
	 }	 
	 
	 public String toString() {
		 Node crunchifyCurrent = head.getNext();
		 String output = "";
		 while (crunchifyCurrent != null) {
			 output += "[" + crunchifyCurrent.getData().toString() + "]";
			 crunchifyCurrent = crunchifyCurrent.getNext();
		 }
		 return output;
	 }	 
	 
	 
	 public static void main(String[] args) {
		 
		 LinkedList lList = new LinkedList();
		 
	        // add elements to LinkedList
	        lList.add("1");
	        lList.add("2");
	        lList.add("3");
	        lList.add("4");
	        lList.add("5");
	 
	        /*
	         * Please note that primitive values can not be added into LinkedList
	         * directly. They must be converted to their corresponding wrapper
	         * class.
	         */
	 
	        System.out.println("lList - print linkedlist: " + lList);
	        System.out.println("lList.size() - print linkedlist size: " + lList.size());
	        System.out.println("lList.get(3) - get 3rd element: " + lList.get(3));
	        System.out.println("lList.remove(2) - remove 2nd element: " + lList.remove(2));
	        System.out.println("lList.get(3) - get 3rd element: " + lList.get(3));
	        System.out.println("lList.size() - print linkedlist size: " + lList.size());
	        System.out.println("lList - print linkedlist: " + lList);
	    }		 
	
	}
	 




class Node {
    
	Node next; // Reference to the next node in the chain, or null if there isn't one.
    Object data; // Data carried by this node. Could be of any type you need.

    public Node(Object dataValue) { // Node constructor
        next = null;
        data = dataValue;
    }

    public Node(Object dataValue, Node nextValue) { // another Node constructor if we want to specify the node to point to.
        next = nextValue;
        data = dataValue;
    }

    public Object getData() { // these methods should be self-explanatory
        return data;
    }

    public void setData(Object dataValue) {
        data = dataValue;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node nextValue) {
        next = nextValue;
    }
}
