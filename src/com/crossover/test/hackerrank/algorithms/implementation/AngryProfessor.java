package com.crossover.test.hackerrank.algorithms.implementation;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/angry-professor
public class AngryProfessor {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int testcases = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < testcases; i++) {
			String[] constraints = sc.nextLine().split(" ");
			int N = Integer.parseInt(constraints[0]);
			int K = Integer.parseInt(constraints[1]);

			int noOfStuds = 0;

			String[] students = sc.nextLine().split(" ");
			for (int j = 0; j < N; j++) {
				int stud = Integer.parseInt(students[j]);
				if (stud <= 0)
					noOfStuds++;
				if (noOfStuds >= K) {
					System.out.println("NO");
					break;
				}
			}
			if (noOfStuds < K)
				System.out.println("YES");
		}
		sc.close();
	}

}
