package com.crossover.test.hackerrank.algorithms.implementation;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/encryption
public class Encryption {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		String word = sc.next();

		System.out.println(solve(word));
	}

	private static String solve(String word) {
		char[][] squareCode = encrypt(word);
		StringBuilder out = new StringBuilder();

		int maxRows = squareCode.length;
		int maxCols = squareCode[0].length;

		int row = 0;
		int col = 0;

		while (true) {
			if (row >= maxRows) {
				row = 0;
				col++;
				out.append(" ");
			}
			if (col >= maxCols) {
				break;
			}
			char c = squareCode[row][col];
			if (c == '~') {
				row++;
				continue;
			} else {
				out.append(c);
			}
			row++;
		}

		return out.toString().trim();
	}

	private static char[][] encrypt(String word) {
		int len = word.length();
		double row = Math.floor(Math.sqrt((double) len));
		double col = Math.ceil(Math.sqrt((double) len));

		// find smallest possible area where all characters fit into
		if (((int) row * col) < len) {
			double min = Math.min(row, col);
			if (min == row) {
				row++;
			} else {
				col++;
			}
		}

		char[][] squareCode = new char[(int) row][(int) col];
		int k = 0;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if (k < len) {
					squareCode[i][j] = word.charAt(k);
					k++;
				} else {
					squareCode[i][j] = '~';
				}
			}
		}
		return squareCode;
	}

}
