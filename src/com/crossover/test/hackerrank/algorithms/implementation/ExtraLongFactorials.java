package com.crossover.test.hackerrank.algorithms.implementation;

import java.math.BigInteger;
import java.util.Scanner;

// https://www.hackerrank.com/challenges/extra-long-factorials
public class ExtraLongFactorials {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int factorial = input.nextInt();
		BigInteger product = BigInteger.valueOf(factorial);

		for (int i = (factorial - 1); i > 0; i--) {
			product = product.multiply((BigInteger.valueOf(i)));
		}

		System.out.println(product);
	}

}
