package com.crossover.test.hackerrank.algorithms.implementation;

import java.io.BufferedReader;
import java.io.InputStreamReader;


//https://www.hackerrank.com/challenges/matrix-rotation-algo
public class MatrixRotation { // Not the best result. Two test cases didn't pass (8 and 9)
	public static void printTab(int[][] tab, int M, int N) {
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print(tab[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void Rotate(int[][] tab, int M, int N, int R) {
		int max = Math.max(M, N);
		int min = Math.min(M, N);
		int loop = min / 2;

		for (int layer = 0; layer < loop; layer++) {

			int rot = 2 * max + 2 * (min - 2);
			rot = R % rot;

			for (int k = 0; k < rot; k++) {

				int temp = tab[layer][layer];

				for (int i = layer; i < M - 1 - layer; i++) {
					int temp2 = tab[i + 1][layer];
					tab[i + 1][layer] = temp;
					temp = temp2;
				}
				for (int j = layer; j < N - 1 - layer; j++) {
					int temp2 = tab[M - 1 - layer][j + 1];
					tab[M - 1 - layer][j + 1] = temp;
					temp = temp2;
				}
				for (int i = M - 1 - layer; i > layer; i--) {
					int temp2 = tab[i - 1][N - 1 - layer];
					tab[i - 1][N - 1 - layer] = temp;
					temp = temp2;
				}
				for (int j = N - 1 - layer; j > layer; j--) {
					int temp2 = tab[layer][j - 1];
					tab[layer][j - 1] = temp;
					temp = temp2;
				}
			}

			max -= 2;
			min -= 2;
		}
	}

	public static void main(String[] args) {
		try {
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					System.in));
			String s = bf.readLine();
			String tab[] = s.split("\\s+");

			int M = Integer.parseInt(tab[0]);
			int N = Integer.parseInt(tab[1]);
			int R = Integer.parseInt(tab[2]);

			int t_initial[][] = new int[M][N];

			for (int i = 0; i < M; i++) {
				s = bf.readLine();
				for (int j = 0; j < N; j++) {
					String temp[] = s.split("\\s+");
					t_initial[i][j] = Integer.parseInt(temp[j]);
				}
			}

			Rotate(t_initial, M, N, R);

			printTab(t_initial, M, N);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}






// Pyton 3 solution (All test cases passed )
 
 /*
 def rotateMat(N,M,l,rot,rin,mat):
        # rotate each layer
        for level in range(l):
                r = rot % len(rin[level])
                rin[level] = rin[level][r:] + rin[level][:r]
        # fill the array with the rotated elements
        for level in range(l):
                top = (N-1) - 2 * level
                side = (M-1) - 2 * level
                for i in range(top):
                        mat[level][level + i] = rin[level].pop(0)  # right
                for j in range(side):
                        mat[level + j][level + top] = rin[level].pop(0)  # down
                for i in range(top):
                        mat[level + side][level + top - i] = rin[level].pop(0)  # left
                for j in range(side):
                        mat[level + side - j][level] = rin[level].pop(0)  # up
 
def flatRings(M,N,mat,rot):
        # flatten the rings into a list
        l = int(min(N, M)/2)
        rin = [[] for lay in range(l)]
        for level in range(l):
                top = (N-1) - 2 * level
                side = (M-1) - 2 * level
                for i in range(top):  # right
                        rin[level].append(mat[level][level + i])
                for j in range(side):  # down
                        rin[level].append(mat[level + j][level + top])
                for i in range(top):  # left
                        rin[level].append(mat[level + side][level + top - i])
                for j in range(side):  # up
                        rin[level].append(mat[level + side - j][level]) 
        rotateMat(N,M,l,rot,rin,mat)
 
# input
M, N, rot = map(int, input().split())
mat = []
for i in range(M):
        mat.append(list(map(int, input().split())))
flatRings(M,N,mat,rot)
 
# print the rotated matrix
for row in range(M):
        for col in range(N):
                print(mat[row][col], "", end="")
        print()

*/ 
 
