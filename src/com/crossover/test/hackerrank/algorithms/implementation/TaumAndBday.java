package com.crossover.test.hackerrank.algorithms.implementation;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/taum-and-bday
public class TaumAndBday {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		for (int i = 0; i < num; i++) {
			long a = in.nextInt();
			long b = in.nextInt();
			long ac = in.nextInt();
			long bc = in.nextInt();
			long cc = in.nextInt();
			long black = Math.min(ac, bc + cc);
			long white = Math.min(bc, ac + cc);
			long v = black * a + white * b;
			System.out.println(v);
		}
	}

}
