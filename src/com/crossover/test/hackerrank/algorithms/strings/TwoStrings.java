package com.crossover.test.hackerrank.algorithms.strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// https://www.hackerrank.com/challenges/two-strings
public class TwoStrings {

	public static void main(String args[]) throws IOException {// Not the best result. Three test cases didn't pass (3, 4 and 5)

		InputStreamReader read = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(read);
		int t;
		t = Integer.parseInt(in.readLine());
		String[] a = new String[t];
		String[] b = new String[t];
		int i;
		for (i = 0; i < t; i++) {
			a[i] = in.readLine();
			b[i] = in.readLine();
		}
		int k, j, count;
		count = 0;
		for (i = 0; i < t; i++) {
			int l1 = a[i].length();
			int l2 = b[i].length();
			if (t >= 1 && t <= 10 && l1 >= 1 && l1 <= 100000 && l2 >= 1
					&& l2 <= 100000) {
				change_control: 
								
				for (j = 0; j < l1; j++) { 
					for (k = 0; k < l2; k++) {
						if (b[i].charAt(k) == a[i].charAt(j)) {
							count++;
							break change_control;
						}
					}
				}

				if (count == 1)
					System.out.println("YES");
				else
					System.out.println("NO");
				count = 0;
			}
		}
	}

}
