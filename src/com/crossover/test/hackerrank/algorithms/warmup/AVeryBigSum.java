package com.crossover.test.hackerrank.algorithms.warmup;

import java.util.Scanner;


//https://www.hackerrank.com/challenges/a-very-big-sum
public class AVeryBigSum {
	
	
	public static void main(String[] args) {
		
	    Scanner in = new Scanner(System.in);
	    int n = in.nextInt();
	    
	    long[] vars = new long[n];
	    
		for(int i = 0; i < vars.length; i++) {
		  vars[i] = in.nextLong();
		}
	    
	    long sum = 0;
	    
	    for(int j = 0; j < vars.length; j++) {
	        sum += vars[j];
	    }
	    
	    System.out.println(sum);

		
	}


}
