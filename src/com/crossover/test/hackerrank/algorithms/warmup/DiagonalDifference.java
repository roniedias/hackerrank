package com.crossover.test.hackerrank.algorithms.warmup;

import java.util.Scanner;

//https://www.hackerrank.com/challenges/diagonal-difference
public class DiagonalDifference {
	
    public static void main(String[] args) {
    	
        Scanner in=new Scanner(System.in);
		
	    int n;
	    
	    n=in.nextInt();
	    
	    int[][] a = new int[n][n];
	    
	    int i,j;
	    
	    for(i=0;i<n;i++) {
	        for(j=0;j<n;j++) {
	        	a[i][j]=in.nextInt();
	        }
	    }
	    
	    int sumr,suml,sum;
	    j = n - 1;
	    sumr = 0;
	    suml = 0;
	    
	    for(i=0;i<n;i++) {
	        sumr+=a[i][i];
	        suml+=a[i][j];
	        j--;
	    }
	    
	    sum = sumr - suml;
	    
	    System.out.println(Math.abs(sum));
        
    }	
	
}
