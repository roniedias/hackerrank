package com.crossover.test.hackerrank.algorithms.warmup;

import java.util.Scanner;

public class LonelyInteger {
	   
	static int lonelyinteger(int[] a) {
		return 0;
	}
	   
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int res;
	        
		int size = Integer.parseInt(in.nextLine());
		int[] array = new int[size];
		int item;
		String next = in.nextLine();
		String[] next_split = next.split(" ");
	        
		for(int i = 0; i < size; i++) {
			item = Integer.parseInt(next_split[i]);
			array[i] = item;
		}
	        
		res = lonelyinteger(array);
		System.out.println(res);      
	}
	
}
