package com.crossover.test.hackerrank.algorithms.warmup;

import java.util.Scanner;

//https://www.hackerrank.com/challenges/simple-array-sum
public class SimpleArraySum {
	
    public static void main(String[] args) {
    	
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] vars = new int[n];
        
		for(int i = 0; i < vars.length; i++) {
		  vars[i] = in.nextInt();
		}
        
        int sum = 0;
        
        for(int j = 0; j < vars.length; j++) {
            sum += vars[j];
        }
        
        System.out.println(sum);
        

    }


}
